import { Client, Contact, ContactId, Message } from "whatsapp-web.js";
import { User } from "./entities/User";

async function welcomeContact(
  cli: Client,
  chatId: string,
  contactInfo: ContactId
) {
  return await cli.sendMessage(
    chatId,
    `Bienvenido/a @${contactInfo.user}, para que te añadamos al grupo principal, dinos un poco tu edad, más o menos por dónde resides y lo que buscas, no te preocupes, sólo lo hacemos para que no entre cualquiera, una vez te añadamos a la comunidad, no te olvides de presentarte de nuevo ^^`,
    {
      mentions: [contactInfo.user + "@" + contactInfo.server],
    }
  );
}

async function newUserPoll(cli: Client, chatId: string, contact: Contact) {
  const msg = await cli.sendMessage(
    chatId,
    "quereis que " + contact.name + " - " + contact.number + " se una?"
  );

  msg.react("✅");
  return msg;
}

export { welcomeContact, newUserPoll };
