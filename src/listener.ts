import { Client } from "whatsapp-web.js";
import { newUserPoll, welcomeContact } from "./messages";
import { User } from "./entities/User";
import qrCode from "qrcode-terminal";

const cli = new Client({ qrMaxRetries: 5 });

cli.on("qr", (qr) => {
  qrCode.generate(qr, { small: true });
});

cli.on("ready", () => {
  console.log("Client is ready!");
});

cli.on("group_join", async (notification) => {
  if (notification.chatId === process.env["ADMISSION_GROUP"]) {
    const recipients = await notification.getRecipients();

    for (const recipient of recipients) {
      await welcomeContact(cli, notification.chatId, recipient.id);
      const msg = await newUserPoll(
        cli,
        String(process.env["ADMIN_GROUP"]),
        recipient
      );

      User.create({
        id: recipient.id.user + "@" + recipient.id.server,
        phone: String(recipient.number),
        username: String(recipient.name),
        pollId: msg.id.id,
      });
    }
  }
});

cli.on("message_reaction", async (reaction) => {
  if (reaction.id.remote === process.env["ADMIN_GROUP"]) {
    const reactions = await (
      await (
        await cli.getChatById(reaction.id.remote)
      ).fetchMessages({ limit: 25, fromMe: true })
    )
      .find((msg) => msg.id.id === reaction.msgId.id)
      ?.getReactions();

    // TODO: si hay 3 check marks se le añade al principal y se le saca del otro, si hay 3 'X' se le saca del filtro solamente
  }
});

export default cli;
