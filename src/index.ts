import sequelize from "./db";
import cli from "./listener";
import dotenv from "dotenv";

dotenv.config();
sequelize.sync({ alter: true });
cli.initialize();
